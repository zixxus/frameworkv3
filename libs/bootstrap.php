<?php

class Bootstrap
{

    function __construct()
    {
        Session::init();
        $url = isset($_GET['url']) ? $_GET['url'] : null;
        $url = rtrim($url, '/');
        $url = explode('/', $url);
        if (empty($url[0])) {
            $url[0] = DEFAULTPAGE;
        }
//        print_r($url);
        $file = 'controllers/' . $url[0] . '.php';
        if (file_exists($file)) {
            require $file;

        } else {
            require ERRORPAGE;
            $controller = new Error();
            $controller->index();
            return FALSE;
        }

        $controller = new $url[0];
        $controller->loadModel($url[0]);

        if (isset($url[2])) {
            $controller->{$url[1]}($url[2]);
        } else {


            if (isset($url[1])) {

                $controller->{$url[1]}();
            } else {
                $defaultpath = DEFAULTPAGEPATH;
                $controller->$defaultpath();
            }
        }


    }

}
