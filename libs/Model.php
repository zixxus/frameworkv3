<?php

class Model
{

    function __construct()
    {
        $this->db = new Database();
    }

    public function create_command($param)
    {
        //       print_r($param);
        $table = $param['table'];
        $intoitem = '';
        $intoval = '';
        $exec = '';
        $other = $param['other'];
        if ($other == null) {
            $other = '';
        }
        if (isset($param['exec'])) {
            $exec = $param['exec'];
        }
        $c = 0;
        foreach ($param['into'] as $key => $val) {
            if ($c == 0) {
                $intoitem .= "`" . $key . "`";
                $intoval .= "'" . $val . "'";
                $c++;
            } else {
                $intoitem .= ",`" . $key . "`";
                $intoval .= ",'" . $val . "'";
            }
        }

        $sth = $this->db->prepare("INSERT INTO `$table`($intoitem) VALUES ($intoval) $other");


        if ($sth->execute()) {


        } else {
            return $sth->errorCode();
        }
    }

    public function del_command($param)
    {

    }

    public function update_command($param)
    {

//        $data = $this->update_command(array(
//            'table'=>'*',
//            'set'=>array(),
//            'where'=>'1 limit 2',
//        ));



        $table = $param['table'];
               $c = 0;
        foreach ($param['set'] as $key=>$val) {
            if($c == 0){
                $set .= '`'.$key.'`='.$val.'';
                $c++;
            }else{
            $set .= ',`'.$key.'`='.$val.'';
            }
        }

        $where = $param['where'];
        $sth = $this->db->prepare("UPDATE `$table` SET $set WHERE $where ");
        $sth->execute();
        return $sth;
    }

    public function fetch_command($param)
    {
//        $data = $this->model->fetch_command(array(
//            'select'=>'*',
//            'from'=>'items',
//            'where'=>'1 limit 2',
//        ));
        $select = $param['select'];
        $from = $param['from'];
        $where = $param['where'];
        $sth = $this->db->prepare("SELECT $select FROM `$from` WHERE $where");
        $sth->execute();
        return $sth;
    }

    public function relation_command($param)
    {

        $from = $param['from'] . '';
        $where = $param['where'];
        $c = 0;
        $inner = '';
        foreach ($param['inner'] as $key => $value) {
            if ($c == 0) {
                $inner .= '' . $value . '';
                $c++;
            } else {
                $inner .= ', ' . $value . '';

            }
        }
        $on = '';
        $c = 0;
        foreach ($param['on'] as $key => $value) {
            if ($c == 0) {
                $on .= '' . $key . '=' . $value . ' ';
                $c++;
            } else {
                $on .= 'AND ' . $key . '=' . $value . ' ';

            }
        }

        $sth = $this->db->prepare("SELECT * FROM `$from` INNER JOIN ($inner) ON $on WHERE $where");

        $sth->execute();
        $data = $sth->fetchAll(PDO::FETCH_ASSOC);

        return $data;
    }

    public function lastid($param)
    {


        return $this->db->lastInsertId($param);

    }

}

//Array ( [0] => Array ( [id] => 1 [0] => 1 [title] => title1 [1] => title1 [textarea] => text1 [2] => text1 [users_id] => 1 [3] => 1 [4] => 1 [username] => user [5] => user [pass] => ee11cbb19052e40b07aac0ca060c23ee [6] => ee11cbb19052e40b07aac0ca060c23ee [role] => default [7] => default ) [1] => Array ( [id] => 2 [0] => 2 [title] => title2 [1] => title2 [textarea] => t2 [2] => t2 [users_id] => 2 [3] => 2 [4] => 2 [username] => admin [5] => admin [pass] => 21232f297a57a5a743894a0e4a801fc3 [6] => 21232f297a57a5a743894a0e4a801fc3 [role] => owner [7] => owner ) ) 