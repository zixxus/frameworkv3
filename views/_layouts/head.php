<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
<head>
    <title>WMB</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="<?php echo sitecss; ?>book.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo sitecss; ?>myaccount.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo sitecss; ?>wstazki.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo sitecss; ?>lightbox.css"/>
    <script type="text/javascript" src="<?php echo sitejs; ?>jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="<?php echo sitejs; ?>jquery-ui.js"></script>
    <script type="text/javascript" src="<?php echo sitejs; ?>book.js"></script>
    <script type="text/javascript" src="<?php echo sitejs; ?>lightbox.js"></script>
</head>
<body>


<div id="content">

    <div id="book">
<?php require 'menu.php'; ?>