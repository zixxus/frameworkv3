<?php

class Help extends Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->model->getdb();
        $this->view->render('help/index');
    }

    public function other($arg = false) {

        $this->view->msg = '<h1>this is a help controler to send helpindex view with: ' . $arg . '</h1>';
        $this->view->render('help/help');
    }

}
