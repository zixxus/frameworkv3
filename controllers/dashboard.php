<?php

class Dashboard extends Controller {

    function __construct() {
        parent::__construct();
        Session::checklogged();
    }

    public function index() {

        $datas = $this->model->getData();
        $this->view->datas = $datas;
        $this->view->render('dashboard/index');
    }

    public function logout() {
        Session::destroy();
        $link = HOMEPAGE . DEFAULTPAGE;
        header("location:$link");
        exit;
    }

    public function insertData() {
        $this->model->insertData();
    }

//    public function getData() {
//        $this->model->getData();
//        
//    }
}
