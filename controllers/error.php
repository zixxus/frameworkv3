<?php

class Error extends Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->view->msg = 'ale przynajmniej dziala';
        $this->view->render('error/error');
    }

    public function loggin() {
        $this->view->render('error/loggin');
    }

}
