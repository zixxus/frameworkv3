<?php

class Homepage extends Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $array_tab = array(
            'from'=>'homepage_post',
            'inner'=>'users',
            'on'=>'homepage_post.users_id = users.id',
            
        );
        $fetchdata = array(
            'select'=>'*',
            'from'=>'relations',
            'where'=>'1',
            
        );

        $this->view->datas = $this->model->relation_command($array_tab);
        $this->view->datadwa = $this->model->fetch_command($fetchdata);
        $this->view->render('homepage/index');
    }

    public function variable() {
        $data = $this->model->fetch_command(array(
            'select'=>'*',
            'from'=>'items',
            'where'=>'item_name IS NOT NULL',
        ));
        print_r($data);
        $this->view->outdata = $data;
        $this->view->render('homepage/home');
    }
    public function make() {
        
        $this->model->create_command(
               array(
            'table'=>'testinsert',
          'into'=>array(
              'id'=>'NULL',
              'name'=>$_POST['name'],
          ),
        ));
        
    }

}
