$(window).load(function () {
    function positionmake() {
        var windoww = $(window).width(),
            windowh = $(window).height();

        $('#bodyimage').width(windoww);
        $('#bodyimage').height(windowh);

        $('#centersite').width(windoww * 0.6);
        $('#centersite').height(windowh);

        var csitew = $('#centersite').width(),
            csiteh = $('#centersite').height();

        $('#logo').width(csitew);
        $('#logo').height(csiteh * 0.2);

        $('#textpos').width(csitew);
        $('#textpos').height(csiteh * 0.8);

        $('#centersite').css('margin-left', (windoww - csitew) / 2);
    }

    positionmake();
    $(window).resize(positionmake);
});


$(function () {

//    $.get('dashboard/getData',function(o){
//        console.log(o);
////    $('#getData');
//    },'json'); 

    $('#forminsert').submit(function () {
        var url = $(this).attr('action');
        var data = $(this).serialize();
//        console.log(data);

        $.post(url, data, function (o) {
            $('#textpos').append("succes<br />");
        });

        return false;
    });

});