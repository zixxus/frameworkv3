function remake() {


    var contentwidth,
        contentheight,
        minimumwidth = 800,
        minimumheight = 498,
        windoww = $(window).width(),
        windowh = $(window).height();
    contentwidth = $(document).width() * 0.95;
    contentheight = $(document).height() * 0.95;

//   
//   if(contentwidth < minimumwidth || contentheight < minimumheight ){
//       
//   $('#book').html('option1<br />'+contentwidth+'|'+minimumwidth+'|'+contentheight+'|'+minimumheight+'|ww:'+windoww+'|wh:'+windowh);
//   
//   $('#content').width(minimumwidth);
//   $('#content').height(minimumheight);
//   
//   }else if(contentwidth > minimumwidth || contentheight > minimumheight){
//       
//   $('#book').html('option2<br />'+contentwidth+'|'+minimumwidth+'|'+contentheight+'|'+minimumheight);
//   
//   
//   $('#content').width(contentwidth);
//   $('#content').height(contentheight);
//   
//   }else if(contentwidth > minimumwidth || contentheight < minimumheight ){
//       
//   $('#book').html('option3<br />'+contentwidth+'|'+minimumwidth+'|'+contentheight+'|'+minimumheight);
//   
//   $('#content').width(contentwidth);
//   $('#content').height(minimumheight);
//   }else if(contentwidth < minimumwidth || contentheight > minimumheight){
//       
//   $('#book').html('option4<br />'+contentwidth+'|'+minimumwidth+'|'+contentheight+'|'+minimumheight);
//   $('#content').width(minimumwidth);
//   $('#content').height(contentheight);
//   }
//   
//  
    if (windoww > windowh) {
        windowh = windowh * 0.95;
        $('#content').width(windowh * 1.603982301);
        $('#content').height(windowh);

        var mleft = ($(window).width() - (windowh * 1.603982301)) / 2,
            mtop = ($(window).height() - $('#content').height()) / 2;
        $('#content').css('margin-left', mleft);
        $('#content').css('margin-top', mtop);

    } else {
        windoww = windoww * 0.95;
        $('#content').width(windoww);
        $('#content').height(windoww * 1.603982301);
        var mleft = ($(window).width() - (windoww * 1.603982301)) / 2
        mtop = ($(window).height() - $('#content').height()) / 2;
        $('#content').css('margin-left', mleft);
        $('#content').css('margin-top', mtop);
    }


//   $('#book').html($(window).width());

}

function pagetext() {
    var bookwidth = $('#book').width();
    var bookheight = $('#book').height();

    $('#page_text').width(bookwidth * 0.872);
    $('#page_text').height(bookheight * 0.90);
    //$('#page_text').css('margin-left', bookwidth * 0.065);
    $('#page_text').css('margin-top', bookheight * 0.055);
    var page_text_width = $('#page_text').width();
    var page_text_height = $('#page_text').height();


    var text_resolution = (100 * page_text_width) / 950 / 100;
    $('.posts').height(page_text_height * 0.19);
    $('.posts .postimages').css('max-width', $('.posts').width() * 0.2);
    $('.posts .postimages').css('max-height', $('.posts').height() * 0.6);
//    $('.posts').css('font-size',$('.posts').width()*0.05);
//    $('.posts h1').css('font-size',$('.posts').width()*0.05);


    /* MENU BEDZIE USTAWIANE TUTAJ */
    var menus = $('menus');
    var pgtw = $('#page_text').width();
    var contw = $('#content').width();

    menus.width(contw * 0.13987);
    $('menus#left').css('margin-left', bookwidth * -71 / 880);
    $('menus#right').css('margin-right', bookwidth * -71 / 880);
    menus.height(bookheight);
    /* MENU BEDZIE USTAWIANE TUTAJ - SKONCZONE */

    /*buttony*/
    var buttonfirst = $('a#firstleft' + '.ajaxLink');
    buttonfirst.width(bookwidth * 62 / 525);
    buttonfirst.height(bookheight * 57 / 327);
    buttonfirst.css('margin-top', bookheight / 4 / 4);
    buttonfirst.css('margin-left', bookwidth / 1011);

    var buttonsec = $('a#secoundleft' + '.ajaxLink');
    buttonsec.width(bookwidth * 91 / 906);
    buttonsec.height(bookheight * 124 / 565);
    buttonsec.css('margin-left', bookwidth * 5 / 699);

    /*buttony - end*/

    /*buttony - p -start*/
    var buttongetwidth = $('a#firstleft' + '.ajaxLink').width();
    var buttongetheight = $('a#firstleft' + '.ajaxLink').height();
    var pfirst = $("a#firstleft" + ".ajaxLink p");
    pfirst.css('font-size', buttongetwidth * 11 / 109);
    pfirst.css('padding-top', buttongetheight * 44 / 98);


    var psecound = $("a#secoundleft" + ".ajaxLink p");
    psecound.css('font-size', buttongetwidth * 12 / 109);
    psecound.css('padding-top', buttongetheight * 44 / 75);
    /*buttony - p -end*/

}
function loadpage() {

    $('.ajaxLink').click(function (e) {
        e.preventDefault();
        $.ajax({
            type: "GET",
            url: $(this).attr('href'),
            dataType: 'html',
            data: $('this').serialize(),
            success: function (data) {
//           $('#page_text').fadeOut("fast");
                $('#page_text').html(data);
//           $('#page_text').fadeIn("fast");
            }
        });
    });
}


function autoupdate_pagetext() {
    //alert(document.URL);

    setInterval(function () {
        $('#page_text').remove();
        $('#book').load(document.URL + ' #page_text', function () {
            pagetext();
        });

    }, 2000);
}

function autoupdate_all() {
    setInterval(function () {
        $('#content').fadeOut();
        $('body').load(document.URL, function () {
            remake();
            pagetext();
        })
    }, 2000);
}


$(window).load(function () {
    remake();
    pagetext();
    //autoupdate_all();
    $(window).resize(remake);
    $(window).resize(pagetext);

});