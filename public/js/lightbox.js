/**
 * Created by root on 11/1/14.
 */

function openlightbox(element) {

    var getdata;
    $.ajax({
        type: "GET",
        url: element + '#lightbox_bg',
        async: false,
        success: function (text) {
            getdata = text;
        }
    })

    $('#book').prepend(getdata);
    $('#lightbox_bg').fadeIn(1000, function () {
    });

    position();

}

function closelightbox() {
    var lightbox = $('#lightbox_bg');
    lightbox.fadeOut(1000, function () {
        $(this).remove();
    });
}

function position() {
    var width = $(window).width(),
        height = $(window).height(),
        userinterface = $('#userinterface');

    userinterface.width(width * 0.8);
    userinterface.height(height - height * 0.05);
    userinterface.css('margin-left', (width - userinterface.width()) / 2);
    userinterface.css('margin-top', (height - userinterface.height()) / 2);

    /**/
}
$(window).resize(position);


