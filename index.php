<?php

require 'libs/bootstrap.php';
require 'libs/Controller.php';
require 'libs/Model.php';
require 'libs/Database.php';
require 'libs/Session.php';
require 'libs/View.php';

require 'config/db.php';

Session::init();
$bootstrap = new Bootstrap();